-- -----------------------------------------------------------------------------
-- Dropear todas las tablas del esquema. Mas info: http://stackoverflow.com/questions/12403662/drop-all-tables
-- -----------------------------------------------------------------------------
SET FOREIGN_KEY_CHECKS = 0;
SET @tables = NULL;
SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables
  FROM information_schema.tables
  WHERE table_schema = 'cheescuela'; -- specify DB name here.

SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------------------------------------------------------
-- Tablas
-- ----------------------------------------------------------------------------

CREATE TABLE usuario (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(25) NOT NULL,
    password VARCHAR(255) NOT NULL,
    enabled BOOLEAN NOT NULL,
    email VARCHAR(255) NOT NULL,
    miembro_desde DATETIME NOT NULL,
    UNIQUE (username),
    UNIQUE (email)
);

CREATE TABLE rol (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    id_usuario BIGINT NOT NULL,
    rol VARCHAR(255) NOT NULL,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id),
    UNIQUE (id_usuario, rol)
);

CREATE TABLE curso (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    descripcion_corta VARCHAR(3000) NOT NULL,
    descripcion LONGTEXT NOT NULL,
    icono VARCHAR(255),
    color VARCHAR(255),
    duracion_horas INT,
    fecha_creacion DATETIME NOT NULL,
    ultima_modificacion DATETIME NOT NULL,
    creado_por BIGINT NOT NULL,
    FOREIGN KEY (creado_por) REFERENCES usuario(id)
);

CREATE TABLE unidad (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    contenido LONGTEXT NOT NULL,
    fecha_creacion DATETIME NOT NULL,
    ultima_modificacion DATETIME NOT NULL,
    creado_por BIGINT NOT NULL,
    FOREIGN KEY (creado_por) REFERENCES usuario(id)
);

CREATE TABLE leccion (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    id_curso BIGINT NOT NULL,
    id_unidad BIGINT NOT NULL,
    orden INT NOT NULL,
    FOREIGN KEY (id_curso) REFERENCES curso(id),
    FOREIGN KEY (id_unidad) REFERENCES unidad(id),
    UNIQUE (id_curso, id_unidad)
);



