DELETE FROM leccion;
DELETE FROM unidad;
DELETE FROM curso;  
DELETE FROM rol;
DELETE FROM usuario;


-- -----------------------------------------------------------------------------
-- usuario
-- -----------------------------------------------------------------------------
--  Passwords de test: adminadmin
INSERT INTO usuario
(id,    username,   email,                      enabled,    miembro_desde,              password) VALUES
(1,     'zim',      'zim@somospnt.com',         true,       '2016-05-13 00:00:00.00',   '$2a$10$5XBNtEGLzhh0yC3jEj5jjeUtJDROssUu1yNoBsvOdepgEp0fwxvNa');


-- -----------------------------------------------------------------------------
-- rol
-- -----------------------------------------------------------------------------
INSERT INTO rol
(id_usuario,    rol) VALUES
(1,             'ROLE_USER'),
(1,             'ROLE_INSTRUCTOR');


-- -----------------------------------------------------------------------------
-- curso
-- -----------------------------------------------------------------------------
INSERT INTO curso
(id,    creado_por, titulo,                                 icono,                  color,          duracion_horas,     descripcion_corta,                                                                                                                              fecha_creacion,             ultima_modificacion,          descripcion) VALUES
(1,     1,          'Introducción al desarrollo Java EE',   'fa fa-css3',           'bg-primary',   10,                 'Introducción al uso de tecnologías Java SE 8, utilizando TDD como práctica de desarrollo.',                                                    '2016-05-13 00:00:00.00',   '2016-05-13 00:00:00.00',     'Durante este curso se verán conceptos iniciales para el desarrollo de aplicaciones Java EE, utilizando TDD (Desarrollo Guiado por Tests) como práctica para la generación de código.'),
(2,     1,          'Introducción a SQL',                   'fa fa-graduation-cap', 'bg-lightred',  20,                 'Es un curso pensado para que puedas adquirir los conocimientos de PL / SQL e interactuar con la funcionalidad persistida en Base de Datos.',   '2016-05-13 00:00:00.00',   '2016-05-13 00:00:00.00',     'Hoy en día es común que en varios desarrollos se acceda a la base de datos a través de distintos frameworks que nos facilitan la tarea. Sin embargo, es fundamental conocer el funcionamiento básico de las bases de datos con las que interactuamos.');


-- -----------------------------------------------------------------------------
-- unidad
-- -----------------------------------------------------------------------------
INSERT INTO unidad
(id,    creado_por, fecha_creacion,             ultima_modificacion,        titulo,                                                     contenido) VALUES
-- Curso: Introduccion al desarrollo Java EE
(100,   1,          '2016-05-13 00:00:00.00',   '2016-05-13 00:00:00.00',   'Introducción a Spring Framework',                          'Veremos cómo resolver finalmente la inyección de dependencia en forma transparente, usando Spring como framework de aplicación.'),
(101,   1,          '2016-05-13 00:00:00.00',   '2016-05-13 00:00:00.00',   'Hibernate para el acceso a datos',                         'Veremos como usar Hibernate para interactuar con una base de datos, usando objetos.'),
(102,   1,          '2016-05-13 00:00:00.00',   '2016-05-13 00:00:00.00',   'Interceptores y transacciones',                            'Veremos como usar interceptores para demarcar transacciones en nuestros objetos.'),
-- Curso: Introduccion a SQL
(200,   1,          '2016-05-13 00:00:00.00',   '2016-05-13 00:00:00.00',   'Introducción a Base de Datos',                             'Con los conceptos básicos de modelización de datos, es mucho más fácil llegar desde las reglas de negocio a las estructuras de bases de datos.'),
(201,   1,          '2016-05-13 00:00:00.00',   '2016-05-13 00:00:00.00',   'Introducción a SQL (Structured Query Language)',           'Si logramos interpretar la estructura de las tablas de una base de datos, cuales son sus componentes y la sintáxis básica de consulta, logramos explotar la información almacenada.'),
(202,   1,          '2016-05-13 00:00:00.00',   '2016-05-13 00:00:00.00',   'Recuperación de Datos',                                    'Es muy común que en el trabajo cotidiano necesitemos recuperar información en particular. Veamos un poco acerca de las cláusulas más usadas en la recuperación de datos.');


-- -----------------------------------------------------------------------------
-- leccion
-- -----------------------------------------------------------------------------
INSERT INTO leccion
(id,    id_curso,   id_unidad,  orden) VALUES 
-- Curso: Introduccion al desarrollo Java EE
(1,     1,          100,          1),
(2,     1,          101,          2),
(3,     1,          102,          3),
-- Curso: Introduccion a SQL
(4,     2,          200,          1),
(5,     2,          201,          2),
(6,     2,          202,          3);

