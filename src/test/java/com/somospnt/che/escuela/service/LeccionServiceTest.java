package com.somospnt.che.escuela.service;

import com.somospnt.che.escuela.CheEscuelaAbstractTest;
import com.somospnt.che.escuela.domain.Leccion;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;

public class LeccionServiceTest extends CheEscuelaAbstractTest {

    @Autowired
    private LeccionService leccionService;

    private Leccion crearLeccion(long idLeccion, int orden) {
        Leccion leccion = new Leccion();
        leccion.setId(idLeccion);
        leccion.setOrden(orden);
        return leccion;
    }

    @Test
    @WithMockUser(username = "instructor", roles = "INSTRUCTOR")
    public void guardar_conCursoQueTieneLecciones_guardaLeccion() {
        Long idCurso = 1L;
        Long idUnidad = 200L;
        long filasAntes = jdbcTemplate.queryForObject("select count(*) from leccion", Long.class);

        Leccion leccion = leccionService.guardar(idUnidad, idCurso);

        long filasDespues = jdbcTemplate.queryForObject("select count(*) from leccion", Long.class);

        assertNotNull(leccion);
        assertEquals(filasAntes + 1, filasDespues);
        assertTrue(leccion.getOrden() > 0);
    }

    @Test
    @WithMockUser(username = "instructor", roles = "INSTRUCTOR")
    public void guardar_conCursoNuevoSinLecciones_guardaLeccion() {
        Long idCurso = 3L;
        Long idUnidad = 200L;
        long filasAntes = jdbcTemplate.queryForObject("select count(*) from leccion", Long.class);

        Leccion leccion = leccionService.guardar(idUnidad, idCurso);

        long filasDespues = jdbcTemplate.queryForObject("select count(*) from leccion", Long.class);

        assertNotNull(leccion);
        assertEquals(filasAntes + 1, filasDespues);
        assertEquals(1, leccion.getOrden());
    }

    @Test(expected = AccessDeniedException.class)
    @WithMockUser(username = "normal", roles = "USER")
    public void guardar_conUsuarioNormal_lanzaException() {
        leccionService.guardar(Long.MIN_VALUE, Long.MIN_VALUE);
    }

    @Test
    @WithMockUser(username = "instructor", roles = "INSTRUCTOR")
    public void actualizarOrden_conUsuarioInstructor_actualizaOrden() {
        List<Leccion> lecciones = new ArrayList<>();
        lecciones.add(crearLeccion(1, 3));
        lecciones.add(crearLeccion(2, 2));
        lecciones.add(crearLeccion(3, 1));

        leccionService.actualizarOrden(lecciones);

        assertEquals(3, leccionService.buscarPorId(1).getOrden());
        assertEquals(2, leccionService.buscarPorId(2).getOrden());
        assertEquals(1, leccionService.buscarPorId(3).getOrden());
    }

    @Test(expected = AccessDeniedException.class)
    @WithMockUser(username = "normal", roles = "USER")
    public void actualizarOrden_conUsuarioNormal_lanzaException() {
        leccionService.actualizarOrden(new ArrayList());
    }
}
