package com.somospnt.che.escuela.service;

import com.somospnt.che.escuela.CheEscuelaAbstractTest;
import com.somospnt.che.escuela.CheEscuelaApplication;
import com.somospnt.che.escuela.domain.Unidad;
import com.somospnt.che.escuela.repository.UnidadRepository;
import com.somospnt.che.escuela.repository.UsuarioRepository;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;
import org.springframework.security.test.context.support.WithMockUser;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CheEscuelaApplication.class)
@WebAppConfiguration
public class UnidadServiceTest extends CheEscuelaAbstractTest {

    @Autowired
    private UnidadService unidadService;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Test
    public void buscarPorId_UnidadExiste_retornaLaUnidad() {
        long id = 100;
        Unidad unidad = unidadService.buscarPorId(id);
        assertNotNull(unidad);
        assertEquals(id, unidad.getId().longValue());
    }

    @Test
    public void buscarPorId_UnidadNoExiste_retornaLaUnidad() {
        Unidad unidad = unidadService.buscarPorId(-100);
        assertNull(unidad);
    }

    @Test
    public void buscarTodos_hayUnidades_retornaUnidades() {
        List<Unidad> unidades = unidadService.buscarTodos();
        assertNotNull(unidades);
        assertFalse(unidades.isEmpty());
    }

    @Test
    @WithMockUser(username = "instructor", roles = "INSTRUCTOR")
    public void guardar_UnidadOk_guardaLaUnidad() {
        Unidad unidad = new Unidad();
        unidad.setContenido("Un contenido");
        unidad.setTitulo("Un Titulo");
        unidad.setCreadoPor(usuarioRepository.findOne(1L));
        long filasAntes = jdbcTemplate.queryForObject("select count(*) from unidad", Long.class);

        unidadService.guardar(unidad);
        entityManager.flush();

        long filasDespues = jdbcTemplate.queryForObject("select count(*) from unidad", Long.class);

        assertEquals(filasAntes+1, filasDespues);

    }

    @Test
    @WithMockUser(username = "instructor", roles = "INSTRUCTOR")
    public void guardar_UnidadExistente_ActualizaUnidad() {
        Unidad unidad = new Unidad();
        unidad.setId(100L);
        unidad.setContenido("Un contenido");
        unidad.setTitulo("Un Titulo");
        unidad.setCreadoPor(usuarioRepository.findOne(1L));
        long filasAntes = jdbcTemplate.queryForObject("select count(*) from unidad", Long.class);

        unidadService.guardar(unidad);
        entityManager.flush();

        long filasDespues = jdbcTemplate.queryForObject("select count(*) from unidad", Long.class);

        assertEquals(filasAntes, filasDespues);

    }

}
