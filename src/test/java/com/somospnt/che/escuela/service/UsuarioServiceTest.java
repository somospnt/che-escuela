package com.somospnt.che.escuela.service;

import com.somospnt.che.escuela.CheEscuelaAbstractTest;
import com.somospnt.che.escuela.domain.Usuario;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import static org.junit.Assert.assertNotNull;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

public class UsuarioServiceTest extends CheEscuelaAbstractTest {

    @Autowired
    private UsuarioService usuarioService;
    
    @Test
    @WithMockUser(username = "instructor")
    public void buscarUsuarioLogueado_conUsuarioLogueado_retornausuario() {
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        assertNotNull(usuario);
    }
    
    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void buscarUsuarioLogueado_conUsuarioNoLogueado_lanzaException() {
        usuarioService.buscarUsuarioLogueado();
    }
    

    
}
