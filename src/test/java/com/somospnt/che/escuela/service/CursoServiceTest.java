package com.somospnt.che.escuela.service;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.List;
import com.somospnt.che.escuela.CheEscuelaAbstractTest;
import com.somospnt.che.escuela.domain.Curso;
import com.somospnt.che.escuela.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;

public class CursoServiceTest extends CheEscuelaAbstractTest {

    @Autowired
    private CursoService cursoService;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Test
    public void buscarTodos_sinParametros_devuelveTodosLosCursos() {
        List<Curso> cursos = cursoService.buscarTodos();
        assertEquals(3, cursos.size());
    }

    @Test
    public void buscarPorId_conIdValido_devuelveElCurso() {
        Curso curso = cursoService.buscarPorId(1L);
        assertNotNull(curso);
        assertEquals((long) 1, (long) curso.getId());
    }

    @Test
    @WithMockUser(username = "instructor", roles = "INSTRUCTOR")
    public void guardar_cursoOk_guardaCurso() {
        Curso curso = new Curso();
        curso.setCreadoPor(usuarioRepository.findOne(1L));
        curso.setDescripcion("Un Curso");
        curso.setTitulo("Un Titulo");
        curso.setDescripcionCorta("Descripcion corta");

        long filasAntes = jdbcTemplate.queryForObject("select count(*) from curso", Long.class);

        cursoService.guardar(curso);
        entityManager.flush();

        long filasDespues = jdbcTemplate.queryForObject("select count(*) from curso", Long.class);

        assertEquals(filasAntes + 1, filasDespues);
        assertEquals("instructor", curso.getCreadoPor().getUsername());
    }

    @Test(expected = AccessDeniedException.class)
    @WithMockUser(username = "normal", roles = "USER")
    public void guardar_conUser_lanzaException() {
        cursoService.guardar(null);
    }

    @Test
    @WithMockUser(username = "instructor", roles = "INSTRUCTOR")
    public void guardar_conInstructor_actualizaCurso() {
        Curso curso = new Curso();
        curso.setId(1L);
        curso.setCreadoPor(usuarioRepository.findOne(1L));
        curso.setDescripcion("Un Curso modificado");
        curso.setDescripcionCorta("Un Curso corto modificado");
        curso.setTitulo("Un Titulo modificado");

        long filasAntes = jdbcTemplate.queryForObject("select count(*) from curso", Long.class);

        cursoService.guardar(curso);
        entityManager.flush();

        long filasDespues = jdbcTemplate.queryForObject("select count(*) from curso", Long.class);

        assertEquals(filasAntes, filasDespues);
    }

    @Test(expected = AccessDeniedException.class)
    @WithMockUser(username = "normal", roles = "USER")
    public void actualizar_conUser_lanzaException() {
        cursoService.guardar(null);
    }

}
