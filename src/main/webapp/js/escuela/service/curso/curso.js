escuela.service.curso = (function () {

    function guardar(curso) {
        var url = escuela.service.url() + "curso";
        return escuela.service.post(url, curso);
    }
    
    function actualizar(curso) {
        var url = escuela.service.url() + "curso";
        return escuela.service.put(url, curso);
    }

    return {
        guardar: guardar,
        actualizar: actualizar
    };
})();