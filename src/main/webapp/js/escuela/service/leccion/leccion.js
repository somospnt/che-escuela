escuela.service.leccion = (function () {

    function actualizarOrden(lecciones) {
        var url = escuela.service.url() + "lecciones/orden";
        return escuela.service.put(url, lecciones);
    }

    return {
        actualizarOrden: actualizarOrden
    };
})();