escuela.ui.editarCurso = (function () {
    function init() {
        irAlTabActivoPorUrl();
        initIconPicker();
        initListaOrdenableDeLecciones();
    }

    function initIconPicker() {
        $('.icp').iconpicker({
            title: 'Buscá un ícono para el curso',
            hideOnSelect: true
        });
    }

    function irAlTabActivoPorUrl() {
        var hash = window.location.hash;
        if (hash) {
            $('#tabs a[href="' + hash + '"]').tab('show');
        }
    }

    function initListaOrdenableDeLecciones() {
        var $lecciones = $(".js-lecciones-nestable");
        $lecciones.nestable({
            rootClass: 'js-lecciones-nestable',
            listNodeName: 'ul',
            listClass: 'nestable-list',
            itemClass: 'nestable-item',
            dragClass: 'nestable-drag',
            handleClass: 'nestable-handle',
            collapsedClass: 'nestable-collapsed',
            placeClass: 'nestable-placeholder',
            emptyClass: 'nestable-empty',
            maxDepth: 1
        });

        $lecciones.on('change', function () {
            var i;
            var lecciones = $lecciones.nestable("serialize");
            for (i = 0; i < lecciones.length; i++) {
                lecciones[i].orden = i + 1;
            }

            escuela.service.leccion.actualizarOrden(lecciones)
                    .done(function () {
                        escuela.ui.mostrarMensajeExito("Se guardó el órden en la lsita de lecciones actualizada.");
                    })
                    .fail(escuela.ui.mostrarMensajeError);
        });
    }

    return {
        init: init
    };

})();

$(document).ready(function () {
    escuela.ui.editarCurso.init();
});