escuela.ui = (function () {

    /**
     * Denmora la ejeuccion de una funcion una cierta cantidad de tiempo (en
     * milisegundos). Si se realiza otra invocacion antes de este tiempo,
     * se cancela la primer invocación, se resetea el timer y se pone la nueva
     * invocacion en espera.
     * Por ejemplo, es útil para "ejecutar una búsqueda 2 segundos después del
     * ultimo keypress".
     * Leer mas: http://stackoverflow.com/questions/4364729/jquery-run-code-2-seconds-after-last-keypress
     *
     * @param f la funcion a invocar.
     * @param delay la demora en milisegundos. Default: 500.
     */
    function throttle(f, delay) {
        var timer = null;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = window.setTimeout(function () {
                f.apply(context, args);
            },
                    delay || 500);
        };
    }

    /** Para configurar, ver: http://www.keenthemes.com/preview/metronic/theme/templates/admin3/ui_toastr.html */
    function mostrarMensajeError(response) {
        var mensaje;
        if (response && response.responseJSON && response.responseJSON.message) {
            mensaje = response.responseJSON.message.substr(response.responseJSON.message.indexOf(':') + 1, response.responseJSON.message.lenght);
        } else {
            mensaje = "No se pudo realizar la acción. Probá de nuevo más tarde, y si el problema persiste avisanos!";
        }
//        $.notific8(mensaje, {heading: '¡Ups! Ocurrió un error', theme: "ruby"});
        console.log(mensaje);
    }

    /** Para configurar, ver: http://www.keenthemes.com/preview/metronic/theme/templates/admin3/ui_toastr.html */
    function mostrarMensajeExito(mensaje) {
//        $.notific8(mensaje, {heading: "¡Listo!", life: 5000, theme: "lime"});
        console.log(mensaje);
    }

    function init() {
        initCkeditor();
    }

    function initCkeditor() {
        CKEDITOR.editorConfig = function (config) {
            config.language = 'es';
            config.uiColor = '#F7B42C';
            config.height = 300;
            config.toolbarCanCollapse = true;
        };
        $('.che-escuela-js-ckeditor').each(function () {
            CKEDITOR.replace(this, {
                extraPlugins: 'codesnippet',
                codeSnippet_theme: 'solarized_dark'
            });
        });
        hljs.initHighlightingOnLoad();
    }


    return {
        throttle: throttle,
        mostrarMensajeError: mostrarMensajeError,
        mostrarMensajeExito: mostrarMensajeExito,
        init: init
    };

})();

$(document).ready(function () {
    escuela.ui.init();
});
