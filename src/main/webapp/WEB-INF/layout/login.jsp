<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html class="hide-sidebar ls-bottom-footer" lang="en">

    <head>
        <c:set var="root" scope="request">${pageContext.request.contextPath}</c:set>
        <c:set var="url">${pageContext.request.requestURL}</c:set>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Che Escuela</title>

        <link href="${root}/css/vendor/bootstrap.css" rel="stylesheet">
        <link href="${root}/css/vendor/font-awesome.css" rel="stylesheet">
        <link href="${root}/css/vendor/picto.css" rel="stylesheet">
        <link href="${root}/css/vendor/material-design-iconic-font.css" rel="stylesheet">
        <link href="${root}/css/vendor/datepicker3.css" rel="stylesheet">
        <link href="${root}/css/vendor/jquery.minicolors.css" rel="stylesheet">
        <link href="${root}/css/vendor/railscasts.css" rel="stylesheet">
        <link href="${root}/css/vendor/owl.carousel.css" rel="stylesheet">
        <link href="${root}/css/vendor/slick.css" rel="stylesheet">
        <link href="${root}/css/vendor/daterangepicker-bs3.css" rel="stylesheet">
        <link href="${root}/css/vendor/jquery.bootstrap-touchspin.css" rel="stylesheet">
        <link href="${root}/css/vendor/select2.css" rel="stylesheet">
        <link href="${root}/css/vendor/jquery.countdown.css" rel="stylesheet">

        <link href="${root}/css/app/app.css" rel="stylesheet">

    </head>

    <body class="login">

        <div id="content">
            <div class="container-fluid">

                <div class="lock-container">
                    <div class="panel panel-default text-center paper-shadow" data-z="0.5">
                        <h1 class="text-display-1 text-center margin-bottom-none">Ingresar</h1>
                        <img src="images/avatar.png" class="img-circle width-80">
                        <form method="POST" action="login">
                            <fieldset>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="form-control-material">
                                            <input class="form-control" autofocus="autofocus" name="username" type="text" placeholder="Usuario">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-control-material">
                                            <input class="form-control" name="password" type="password" placeholder="Contraseña">
                                        </div>
                                    </div>
                                    <input type="hidden" name="remember-me" value="on">
                                    <button type="submit" class="btn btn-primary">Ingresar&nbsp;&nbsp;&nbsp;<i class="fa fa-sign-in"></i></button>
                                </div>
                            </fieldset>
                        </form>
                    </div>

                </div>

            </div>
        </div>

        <!-- Inline Script for colors and config objects; used by various external scripts; -->
        <script>
            var colors = {
                "danger-color": "#e74c3c",
                "success-color": "#81b53e",
                "warning-color": "#f0ad4e",
                "inverse-color": "#2c3e50",
                "info-color": "#2d7cb5",
                "default-color": "#6e7882",
                "default-light-color": "#cfd9db",
                "purple-color": "#9D8AC7",
                "mustard-color": "#d4d171",
                "lightred-color": "#e15258",
                "body-bg": "#f6f6f6"
            };
            var config = {
                theme: "html",
                skins: {
                    "default": {
                        "primary-color": "#42a5f5"
                    }
                }
            };
        </script>

        <!-- Vendor Scripts Bundle
          Includes all of the 3rd party JavaScript libraries above.
          The bundle was generated using modern frontend development tools that are provided with the package
          To learn more about the development process, please refer to the documentation.
          Do not use it simultaneously with the separate bundles above. -->

        <script src="${root}/js/vendor/core/jquery.js"></script>
        <script src="${root}/js/vendor/core/bootstrap.js"></script>
        <script src="${root}/js/vendor/core/breakpoints.js"></script>
        <script src="${root}/js/vendor/core/jquery.nicescroll.js"></script>
        <script src="${root}/js/vendor/core/isotope.pkgd.js"></script>
        <script src="${root}/js/vendor/core/packery-mode.pkgd.js"></script>
        <script src="${root}/js/vendor/core/jquery.grid-a-licious.js"></script>
        <script src="${root}/js/vendor/core/jquery.cookie.js"></script>
        <script src="${root}/js/vendor/core/jquery-ui.custom.js"></script>
        <script src="${root}/js/vendor/core/jquery.hotkeys.js"></script>
        <script src="${root}/js/vendor/core/handlebars.js"></script>
        <script src="${root}/js/vendor/core/jquery.hotkeys.js"></script>
        <script src="${root}/js/vendor/core/load_image.js"></script>
        <script src="${root}/js/vendor/core/jquery.debouncedresize.js"></script>
        <script src="${root}/js/vendor/core/modernizr.js"></script>
        <script src="${root}/js/vendor/core/velocity.js"></script>
        <script src="${root}/js/vendor/tables/all.js"></script>
        <script src="${root}/js/vendor/forms/all.js"></script>
        <script src="${root}/js/vendor/media/slick.js"></script>
        <script src="${root}/js/vendor/charts/flot/all.js"></script>
        <script src="${root}/js/vendor/nestable/jquery.nestable.js"></script>
        <script src="${root}/js/vendor/countdown/all.js"></script>

        <script src="${root}/js/app/app.js"></script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-77715271-2', 'auto');
            ga('send', 'pageview');

        </script>

    </body>

</html>