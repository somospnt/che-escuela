<!DOCTYPE html>
<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer">
    <head>
        <c:set var="root" scope="request">${pageContext.request.contextPath}</c:set>
        <c:set var="url">${pageContext.request.requestURL}</c:set>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Che Escuela</title>

        <link href="${root}/css/vendor/bootstrap.css" rel="stylesheet">
        <link href="${root}/css/vendor/font-awesome.css" rel="stylesheet">
        <link href="${root}/css/vendor/picto.css" rel="stylesheet">
        <link href="${root}/css/vendor/material-design-iconic-font.css" rel="stylesheet">
        <link href="${root}/css/vendor/datepicker3.css" rel="stylesheet">
        <link href="${root}/css/vendor/jquery.minicolors.css" rel="stylesheet">
        <link href="${root}/css/vendor/railscasts.css" rel="stylesheet">
        <link href="${root}/css/vendor/owl.carousel.css" rel="stylesheet">
        <link href="${root}/css/vendor/slick.css" rel="stylesheet">
        <link href="${root}/css/vendor/daterangepicker-bs3.css" rel="stylesheet">
        <link href="${root}/css/vendor/jquery.bootstrap-touchspin.css" rel="stylesheet">
        <link href="${root}/css/vendor/select2.css" rel="stylesheet">
        <link href="${root}/css/vendor/jquery.countdown.css" rel="stylesheet">

        <link href="${root}/css/vendor/fontawesome-iconpicker/fontawesome-iconpicker.min.css" rel="stylesheet">
        
        <link href="${root}/js/vendor/ckeditor/plugins/codesnippet/lib/highlight/styles/obsidian.css" rel="stylesheet">

        <link href="${root}/css/app/app.css" rel="stylesheet">

        <!-- Hoja de estilos de Che Escuelta -->
        <link href="${root}/css/main.css" rel="stylesheet">
        

    </head>
    <body>

        <div class="navbar navbar-default navbar-fixed-top navbar-size-large navbar-size-xlarge paper-shadow" data-z="0" data-animated role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-brand navbar-brand-logo">
                        <a class="text-pink-400" href="${root}">
                            Che Escuela
                        </a>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="main-nav">
                    <ul class="nav navbar-nav navbar-nav-margin-left">
                        <li>
                            <a href="${root}/cursos">Cursos</a>
                        </li>
                        <li>
                            <a href="${root}/unidades">Unidades</a>
                        </li>
                    </ul>
                    <c:if test="${not empty pageContext.request.userPrincipal}">
                        <div class="navbar-right">
                            <ul class="nav navbar-nav  ">
                                <!-- user -->
                                <li class="dropdown user">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="images/avatar.png" alt="" class="img-circle" /> ${usuarioLogeado.username}<span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="logout"><i class="fa fa-sign-out"></i> Salir</a></li>
                                    </ul>
                                </li>
                                <!-- // END user -->
                            </ul>
                        </div>
                    </c:if>
                </div>


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="main-nav">
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </div>

        <tiles:insertAttribute name="body" />

        <section class="footer-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-headline text-light">Che Escuela</h4>

                        <p class="text-subhead">
                            Ideado y desarrollado por <a href="http://www.somospnt.com" target="_blank">PNT</a>, el equipo de desarrollo de <a href="http://www.connectis-ict.com.ar" target="_blank">Connectis Argentina</a>.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <!-- Inline Script for colors and config objects; used by various external scripts; -->
        <script>
            var colors = {
                "danger-color": "#e74c3c",
                "success-color": "#81b53e",
                "warning-color": "#f0ad4e",
                "inverse-color": "#2c3e50",
                "info-color": "#2d7cb5",
                "default-color": "#6e7882",
                "default-light-color": "#cfd9db",
                "purple-color": "#9D8AC7",
                "mustard-color": "#d4d171",
                "lightred-color": "#e15258",
                "body-bg": "#f6f6f6"
            };
            var config = {
                theme: "html",
                skins: {
                    "default": {
                        "primary-color": "#42a5f5"
                    }
                }
            };
        </script>

        <!-- Vendor Scripts Bundle
          Includes all of the 3rd party JavaScript libraries above.
          The bundle was generated using modern frontend development tools that are provided with the package
          To learn more about the development process, please refer to the documentation.
          Do not use it simultaneously with the separate bundles above. -->

        <script src="${root}/js/vendor/core/jquery.js"></script>
        <script src="${root}/js/vendor/core/bootstrap.js"></script>
        <script src="${root}/js/vendor/core/breakpoints.js"></script>
        <script src="${root}/js/vendor/core/jquery.nicescroll.js"></script>
        <script src="${root}/js/vendor/core/isotope.pkgd.js"></script>
        <script src="${root}/js/vendor/core/packery-mode.pkgd.js"></script>
        <script src="${root}/js/vendor/core/jquery.grid-a-licious.js"></script>
        <script src="${root}/js/vendor/core/jquery.cookie.js"></script>
        <script src="${root}/js/vendor/core/jquery-ui.custom.js"></script>
        <script src="${root}/js/vendor/core/jquery.hotkeys.js"></script>
        <script src="${root}/js/vendor/core/handlebars.js"></script>
        <script src="${root}/js/vendor/core/jquery.hotkeys.js"></script>
        <script src="${root}/js/vendor/core/load_image.js"></script>
        <script src="${root}/js/vendor/core/jquery.debouncedresize.js"></script>
        <script src="${root}/js/vendor/core/modernizr.js"></script>
        <script src="${root}/js/vendor/core/velocity.js"></script>
        <script src="${root}/js/vendor/tables/all.js"></script>
        <script src="${root}/js/vendor/forms/all.js"></script>
        <script src="${root}/js/vendor/media/slick.js"></script>
        <script src="${root}/js/vendor/charts/flot/all.js"></script>
        <script src="${root}/js/vendor/nestable/jquery.nestable.js"></script>
        <script src="${root}/js/vendor/countdown/all.js"></script>

        <script src="${root}/js/vendor/fontawesome-iconpicker/fontawesome-iconpicker.min.js"></script>
        
        <script src="${root}/js/vendor/ckeditor/ckeditor.js"></script>
        <script src="${root}/js/vendor/ckeditor/plugins/codesnippet/lib/highlight/highlight.pack.js"></script>
        
        <script src="${root}/js/app/app.js"></script>

        <!-- Scripts de Che Escuela -->
        <script src="${root}/js/escuela/escuela.js"></script>
        <script src="${root}/js/escuela/service/service.js"></script>
        <script src="${root}/js/escuela/service/curso/curso.js"></script>
        <script src="${root}/js/escuela/service/leccion/leccion.js"></script>
        <script src="${root}/js/escuela/ui/ui.js"></script>
        <tiles:importAttribute name="js" scope="page"/>
        <c:if test="${not empty js}">
            <script src="${root}/${js}"></script>
        </c:if>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-77715271-2', 'auto');
            ga('send', 'pageview');

        </script>
    </body>
</html>
