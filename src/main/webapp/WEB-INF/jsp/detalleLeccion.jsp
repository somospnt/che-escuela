<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="parallax bg-white page-section third">
    <div class="container parallax-layer" data-opacity="true">
        <div class="media v-middle media-overflow-visible">
            <div class="media-left">
                <span class="icon-block s30 ${curso.color}"><i class="fa ${curso.icono} text-white" style="vertical-align: middle"></i></span>
            </div>
            <div class="media-body">
                <div class="text-headline"><a href="${root}/cursos/${curso.id}">${curso.titulo}</a></div>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="page-section">
        <div class="row">

            <div class="col-md-9">

                <div class="page-section padding-top-none">
                    <div class="media media-grid v-middle">
                        <div class="media-left">
                            <span class="icon-block half bg-blue-300 text-white">${leccionActual.orden}</span>
                        </div>
                        <div class="media-body">
                            <h1 class="text-display-1 margin-none">${leccionActual.unidad.titulo}</h1>
                        </div>
                    </div>
                    <br/>
                    <p>${leccionActual.unidad.contenido}</p>
                </div>

                <h5 class="text-subhead-2 text-light">Indice</h5>
                <div class="panel panel-default curriculum open paper-shadow" data-z="0.5">
                    <div class="panel-heading panel-heading-gray" data-toggle="collapse" data-target="#curriculum-1">
                        <div class="media">
                            <div class="media-left">
                                <span class="icon-block img-circle bg-indigo-300 half text-white"><i class="fa fa-graduation-cap"></i></span>
                            </div>
                            <div class="media-body">
                                <h4 class="text-headline">Contenido</h4>
                            </div>
                        </div>
                    </div>
                    <div class="list-group collapse in" id="curriculum-1">
                        <c:forEach items="${curso.lecciones}" var="leccion">
                            <div class="list-group-item media <c:if test="${leccion.orden eq leccionActual.orden}">active</c:if>" data-target="${root}/cursos/${curso.id}/leccion/${leccion.id}">
                                    <div class="media-left">
                                        <div class="text-crt">${leccion.orden}.</div>
                                </div>
                                <div class="media-body">
                                    <c:choose>
                                        <c:when test="${leccion.orden < leccionActual.orden}">
                                            <i class="fa fa-fw fa-circle text-green-300"></i> 
                                        </c:when>
                                        <c:when test="${leccion.orden eq leccionActual.orden}">
                                            <i class="fa fa-fw fa-circle text-blue-300"></i>
                                        </c:when>
                                        <c:when test="${leccion.orden > leccionActual.orden}">
                                            <i class="fa fa-fw fa-circle text-grey-200"></i>
                                        </c:when>
                                    </c:choose>
                                    ${leccion.unidad.titulo}
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>

                <br/>
                <br/>

            </div>
            <div class="col-md-3">

                <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                    <div class="panel-heading panel-collapse-trigger">
                        <h4 class="panel-title">Instructor</h4>
                    </div>
                    <div class="panel-body">
                        <div class="media v-middle">
                            <div class="media-left">
                            </div>
                            <div class="media-body">
                                <h4 class="text-title margin-none"><a href="#">${curso.creadoPor.username}</a></h4>
                                <span class="caption text-light">${curso.creadoPor.email}</span>
                            </div>
                        </div>
                        <br/>
                        <div class="expandable expandable-indicator-white expandable-trigger">
                            <div class="expandable-content">
                                <p>Espero recibir tus dudas, sugerencias y comentarios</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>