<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="parallax overflow-hidden bg-blue-400 page-section third">
    <div class="container parallax-layer" data-opacity="true">
        <div class="media v-middle">
            <div class="media-body">
                <c:if test="${empty unidad.titulo}">
                    <h1 class="text-white text-display-1 margin-v-0">Cre� una nueva unidad</h1>                
                </c:if>
                <c:if test="${not empty unidad.titulo}">
                    <h1 class="text-white text-display-1 margin-v-0"><c:out value="${unidad.titulo}"/></h1>                                    
                </c:if>
                <p class="text-white text-subhead">
                    <c:if test="${idCurso == null}">
                        Agreg� los detalles de la unidad de conocimiento
                    </c:if>
                    <c:if test="${idCurso != null}">
                        <a class="text-white" href="cursos/editar/${idCurso}#lecciones"><i class="fa fa-chevron-left"></i> Volver al curso</a>
                    </c:if>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-12">
                <!-- Tabbable Widget -->
                <div class="tabbable paper-shadow relative" data-z="0.5">
                    <!-- Tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#unidad" data-toggle="tab">
                                <i class="fa fa-fw fa-home"></i> 
                                <span class="hidden-sm hidden-xs">Detalle de la unidad</span>
                            </a>
                        </li>
                    </ul>
                    <!-- // END Tabs -->

                    <!-- Panes -->
                    <div class="tab-content">
                        <!-- ---------------------------------------------------
                        Formulario del curso
                   ---------------------------------------------------- -->
                        <div id="unidad" class="tab-pane active">
                            <form id="formCurso" action="${root}/unidades" method="POST"> 
                                <input type="hidden" name="id" value="<c:out value="${unidad.id}"/>"/>
                                <input type="hidden" name="idCurso" value="${idCurso}"/>
                                <div class="form-group ">
                                    <label>T�tulo</label>
                                    <input name="titulo" placeholder="T�tulo descriptivo de la unidad" class="form-control used" type="text" autofocus value="<c:out value="${unidad.titulo}"/>">
                                    <span class="ma-form-highlight"></span><span class="ma-form-bar"></span>
                                </div>
                                <div class="form-group">
                                    <label>Contenido</label>
                                    <textarea name="contenido" rows="10" cols="30" class="che-escuela-js-ckeditor"><c:out value="${unidad.contenido}"/></textarea>
                                </div>
                                <div class="text-right">
                                    <input type="submit" class="btn btn-primary" value="Guardar">
                                </div>
                            </form>
                        </div>

                    </div>
                    <!-- // END Panes -->
                </div>
                <!-- // END Tabbable Widget -->
                <br>

            </div>
        </div>
    </div>

</div>