<div class="parallax cover overlay cover-image-full home">
    <img class="parallax-layer" src="images/photodune-4161018-group-of-students-m.jpg" alt="Learning Cover" />
    <div class="parallax-layer overlay overlay-full overlay-bg-white bg-transparent" data-speed="8" data-opacity="true">
        <div class="v-center">
            <div class="page-section overlay-bg-white-strong relative paper-shadow" data-z="1">
                <h1 class="text-display-2 margin-v-0-15 display-inline-block">La plataforma educativa<br/>para tu empresa</h1>
                <p class="text-subhead">Todas las capacitaciones internas de tu empresa<br/>accesibles siempre desde cualquier lugar.</p>
                <a class="btn btn-green-500 btn-lg paper-shadow" href="cursos">Empez� a crear cursos</a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="page-section-heading">
        <h2 class="text-display-1">Mejor� tus equipos a trav�s del aprendizaje</h2>
        <p class="lead text-muted">Una herramienta para fomentar y acelerar la tranmisici�n de conocmientos</p>
    </div>
    <div class="row" data-toggle="gridalicious">

        <div class="media">
            <div class="media-left padding-none">
                <div class="bg-green-300 text-white">
                    <div class="panel-body">
                        <i class="fa fa-film fa-2x fa-fw"></i>
                    </div>
                </div>
            </div>
            <div class="media-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-headline">Cursos centralizados</div>
                        <p>Todo el conocimiento de tu empresa organizado en un �nico lugar, de r�pido acceso.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="media">
            <div class="media-left padding-none">
                <div class="bg-purple-300 text-white">
                    <div class="panel-body">
                        <i class="fa fa-life-bouy fa-2x fa-fw"></i>
                    </div>
                </div>
            </div>
            <div class="media-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-headline">Soporte Premium</div>
                        <p>Atenci�n por email y tel�fono, directamente con el equipo de desarrollo de Connectis Argentina, desde sus oficinas en Buenos Aires.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="media">
            <div class="media-left padding-none">
                <div class="bg-orange-400 text-white">
                    <div class="panel-body">
                        <i class="fa fa-user fa-2x fa-fw"></i>
                    </div>
                </div>
            </div>
            <div class="media-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-headline">Lecciones para todos</div>
                        <p>Toda tu empresa podr� acceder a todas las capacitaciones, auto-gestionando su aprendizaje.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="media">
            <div class="media-left padding-none">
                <div class="bg-cyan-400 text-white">
                    <div class="panel-body">
                        <i class="fa fa-code fa-2x fa-fw"></i>
                    </div>
                </div>
            </div>
            <div class="media-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-headline">Lecciones a medida</div>
                        <p>Las capacitaciones son creadas y mantenidas por los propios equipos, a su medida y con experiencias reales.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="media">
            <div class="media-left padding-none">
                <div class="bg-pink-400 text-white">
                    <div class="panel-body">
                        <i class="fa fa-print fa-2x fa-fw"></i>
                    </div>
                </div>
            </div>
            <div class="media-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-headline">Reutiliz� conocimientos</div>
                        <p>Los instructores pueden reusar unidades de otros cursos para crear cursos m�s r�pido y con mejor contenido.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="media">
            <div class="media-left padding-none">
                <div class="bg-red-400 text-white">
                    <div class="panel-body">
                        <i class="fa fa-tasks fa-2x fa-fw"></i>
                    </div>
                </div>
            </div>
            <div class="media-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-headline">Quienes ense�an, juntos</div>
                        <p>A todos nos gusta aprender. A muchos nos gusta ense�ar. Che Educaci�n facilita el encuentro en tu empresa.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<br/>

<div class="parallax cover overlay height-300 margin-none">
    <img class="parallax-layer" data-auto-offset="true" data-auto-size="false" src="images/photodune-6745579-modern-creative-man-relaxing-on-workspace-m.jpg" alt="Learning Cover" />
    <div class="overlay overlay-full overlay-bg-white bg-transparent" data-opacity="true" data-speed="8">
        <div class="v-center">
            <div class="page-section">
                <h1 class="text-display-2 overlay-bg-white margin-v-0-15 inline-block">Experiencias reales de usuarios reales</h1>
                <br/>
                <p class="lead text-overlay overlay-bg-white-strong inline-block">La experiencia en Connectis Argentina</p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-4">
                <div class="testimonial">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p>Result� muy simple usar el editor visual de la herramienta para migrar nuestras capacitaciones para ingresantes a la nube.</p>
                        </div>
                    </div>
                    <div class="media v-middle">
                        <div class="media-left">
                            <img src="images/avatar.png" alt="People" class="img-circle width-40">
                        </div>
                        <div class="media-body">
                            <p class="text-subhead margin-v-5-0">
                                <strong>Matias Z. <span class="text-muted">@ Connectis Argentina</span></strong>
                            </p>
                            <p class="small">
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="testimonial">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p>El componente para darle formato al c�digo es lo m�s. Nos permite visualizar porciones de c�digo de forma simple, y facilita el aprendizaje a trav�s de ejemplos.</p>
                        </div>
                    </div>
                    <div class="media v-middle">
                        <div class="media-left">
                            <img src="images/avatar.png" alt="People" class="img-circle width-40">
                        </div>
                        <div class="media-body">
                            <p class="text-subhead margin-v-5-0">
                                <strong>Pablo R. <span class="text-muted">@ Connectis Argentina</span></strong>
                            </p>
                            <p class="small">
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="testimonial">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p>Antes ten�amos el curso para ingresantes en un Moodle. Con esta herramienta podemos mantener el curso de manera simple y colaborativa entre todo el equipo.</p>
                        </div>
                    </div>
                    <div class="media v-middle">
                        <div class="media-left">
                            <img src="images/avatar.png" alt="People" class="img-circle width-40">
                        </div>
                        <div class="media-body">
                            <p class="text-subhead margin-v-5-0">
                                <strong>Leonardo D. S. <span class="text-muted">@ Connectis Argentina</span></strong>
                            </p>
                            <p class="small">
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                                <span class="fa fa-fw fa-star text-yellow-800"></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <br/>
                <br/>
                <a class="btn btn-green-500 btn-lg" href="cursos">Empez� a crear cursos para tus equipos</a>
            </div>
        </div>
    </div>
    <br>

</div>