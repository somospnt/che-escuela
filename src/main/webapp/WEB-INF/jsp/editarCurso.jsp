<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="parallax overflow-hidden bg-blue-400 page-section third">
    <div class="container parallax-layer" data-opacity="true">
        <div class="media v-middle">
            <c:if test="${not empty curso.color}">
                <div class="media-left">
                    <span class="icon-block s60"><i class="text-white fa ${curso.icono}"></i></span>
                </div>
            </c:if>
            <div class="media-body">
                <c:if test="${empty curso.titulo}">
                    <h1 class="text-white text-display-1 margin-v-0">Cre� tu nuevo curso</h1>
                </c:if>
                <c:if test="${not empty curso.titulo}">
                    <h1 class="text-white text-display-1 margin-v-0"><c:out value="${curso.titulo}"/></h1>
                </c:if>
                <p class="text-white text-subhead">Agreg� los detalles del curso, asignale unidades, y publicalo</p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-12">
                <!-- Tabbable Widget -->
                <div class="tabbable paper-shadow relative" data-z="0.5">
                    <!-- Tabs -->
                    <ul id="tabs" class="nav nav-tabs">
                        <li class="active">
                            <a href="#curso" data-toggle="tab">
                                <i class="fa fa-fw fa-home"></i>
                                <span class="hidden-sm hidden-xs">Detalle del curso</span>
                            </a>
                        </li>
                        <c:if test="${curso.id != null}">
                            <li>
                                <a href="#lecciones" data-toggle="tab">
                                    <i class="fa fa-fw fa-credit-card"></i>
                                    <span class="hidden-sm hidden-xs">Lecciones</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                    <!-- // END Tabs -->

                    <!-- Panes -->
                    <div class="tab-content">

                        <!-- ---------------------------------------------------
                        Formulario del curso
                   ---------------------------------------------------- -->
                        <div id="curso" class="tab-pane active">
                            <form action="${root}/cursos" method="POST">
                                <input type="hidden" name="id" value="<c:out value="${curso.id}"/>"/>
                                <div class="form-group ">
                                    <label>T�tulo</label>
                                    <input name="titulo" placeholder="T�tulo descriptivo del curso" class="form-control used" type="text" autofocus value="<c:out value="${curso.titulo}"/>" required="required">
                                    <span class="ma-form-highlight"></span><span class="ma-form-bar"></span>
                                </div>
                                <div class="form-group ">
                                    <label>Descripci�n corta</label>
                                    <input name="descripcionCorta" placeholder="Una breve descripci�n sobre el objetivo del curso" class="form-control used" type="text" value="<c:out value="${curso.descripcionCorta}"/>" required="required">
                                    <span class="ma-form-highlight"></span><span class="ma-form-bar"></span>
                                </div>
                                <div class="form-group">
                                    <label>Icono</label>
                                    <div class="input-group">
                                        <input name="icono" data-placement="bottomRight" class="form-control icp" value="<c:out value="${curso.icono}"/>" type="text" />
                                        <span class="input-group-addon"></span>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label>Color de destaque</label>
                                    <select class="form-control" name="color">
                                        <c:forEach items="${colores}" var="color">
                                            <option class="${color.claseCss} text-white" value="${color.claseCss}" ${color.claseCss == curso.color ? "selected" : ""}>${color.nombre}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="ma-form-highlight"></span><span class="ma-form-bar"></span>
                                </div>
                                <div class="form-group">
                                    <label>Contenido</label>
                                    <textarea name="descripcion" rows="10" cols="30" class="che-escuela-js-ckeditor"><c:out value="${curso.descripcion}"/></textarea>
                                </div>
                                <div class="text-right">
                                    <input type="submit" class="btn btn-primary" value="Guardar y Agregar lecciones">
                                </div>
                            </form>
                        </div>

                        <!-- ---------------------------------------------------
                        Panel de leccioness
                        ---------------------------------------------------- -->
                        <c:if test="${curso.id != null}">
                            <div id="lecciones" class="tab-pane">
                                <div class="media v-middle s-container">
                                    <div class="media-body">
                                        <c:if test="${curso.lecciones.size() > 0}">
                                            <h5 class="text-subhead text-light">${curso.lecciones.size()} lecciones</h5>
                                        </c:if>
                                    </div>
                                    <div class="media-right">
                                        <a href="unidades/nuevo?idCurso=${curso.id}" class="btn btn-primary paper-shadow relative">Agregar lecci�n</a>
                                    </div>
                                </div>

                                <div class="js-lecciones-nestable">
                                    <ul class="nestable-list">
                                        <c:forEach items="${curso.lecciones}" var="leccion">
                                            <li class="nestable-item nestable-item-handle" data-id="${leccion.id}">
                                                <div class="nestable-handle"><i class="md md-menu"></i></div>
                                                <div class="nestable-content">
                                                    <div class="media v-middle">
                                                        <div class="media-left">
                                                            <div class="icon-block half bg-red-400 text-white">
                                                                <i class="fa fa-github"></i>
                                                            </div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="text-title media-heading margin-none">
                                                                <a href="" class="link-text-color"><c:out value="${leccion.unidad.titulo}"/></a>
                                                            </h4>
                                                            <div class="text-caption">Actualizado el <fmt:formatDate value="${leccion.unidad.ultimaModificacion}" pattern="dd-MM-yyyy" /></div>
                                                        </div>
                                                        <div class="media-right">
                                                            <a href="unidades/editar/${leccion.unidad.id}?idCurso=${curso.id}" class="btn btn-white btn-flat"><i class="fa fa-pencil fa-fw"></i> Editar</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </c:if>
                    </div>
                    <!-- // END Panes -->
                </div>
                <!-- // END Tabbable Widget -->
                <br>

            </div>
        </div>
    </div>

</div>