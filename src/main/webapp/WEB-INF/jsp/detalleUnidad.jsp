<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="container">

    <div class="page-section">
        <div class="row">

            <div class="col-md-9">

                <div class="page-section padding-top-none">
                    <div class="media media-grid v-middle">
                        <div class="media-left">
                            <span class="icon-block half bg-pink-300 text-white"><i class="fa fa-bookmark"></i></span>
                        </div>
                        <div class="media-body">
                            <h1 class="text-display-1 margin-none">${unidad.titulo}</h1>
                        </div>
                    </div>
                    <br/>
                    <p>${unidad.contenido}</p>
                </div>

            </div>
            <div class="col-md-3">

                <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
                    <div class="panel-heading panel-collapse-trigger">
                        <h4 class="panel-title">Instructor</h4>
                    </div>
                    <div class="panel-body">
                        <div class="media v-middle">
                            <div class="media-left">
                            </div>
                            <div class="media-body">
                                <h4 class="text-title margin-none"><a href="#">${unidad.creadoPor.username}</a></h4>
                                <span class="caption text-light">${unidad.creadoPor.email}</span>
                            </div>
                        </div>
                        <br/>
                        <div class="expandable expandable-indicator-white expandable-trigger">
                            <div class="expandable-content">
                                <p>Espero recibir tus dudas, sugerencias y comentarios</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>