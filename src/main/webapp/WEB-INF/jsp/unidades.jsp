<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div class="parallax bg-white page-section">
    <div class="container parallax-layer" data-opacity="true">
        <div class="media v-middle">
            <div class="media-body">
                <h1 class="text-display-2 margin-none">Conceptos indivuales</h1>
                <p class="text-light lead">Tenemos que darnos cuenta de que solo aprendemos y retenemos los aprendizajes voluntarios<small>&mdash; Rafael Santandreu</small></p>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="page-section">
        <div class="row">
            <div class="col-md-12">
                <p class="text-right">
                    <a class="btn btn-success" href="unidades/nuevo"><i class="fa fa-plus"></i> Agregar unidad</a>
                </p>

                <c:forEach items="${unidades}" var="unidad">
                    <div class="panel panel-default paper-shadow">
                        <div class="panel-body">

                            <div class="media media-clearfix-xs">
                                <div class="media-left text-center">
                                    <div class="cover width-150 width-100pc-xs overlay cover-image-full hover margin-v-0-10">
                                        <span class="img icon-block height-130"></span>
                                        <span class="overlay overlay-full padding-none icon-block bg-pink-400">
                                            <span class="v-center">
                                                <i class="text-white fa fa-bookmark"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h4 class="text-headline margin-v-5-0"><a href="${root}/unidades/${unidad.id}">${unidad.titulo}</a></h4>

                                    <hr class="margin-v-8" />
                                    <div class="media v-middle">
                                        <div class="media-left">
                                            <img src="images/avatar.png" alt="People" class="img-circle width-40">
                                        </div>
                                        <div class="media-body">
                                            <h4><a href="">${unidad.creadoPor.username}</a>
                                                <br/>
                                            </h4>
                                            Instructor
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
        </div>
    </div>

</div>