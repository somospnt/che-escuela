<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="parallax bg-white page-section">
    <div class="parallax-layer container" data-opacity="true">
        <div class="media v-middle">
            <div class="media-left">
                <span class="icon-block s60 ${curso.color}"><i class="text-white fa ${curso.icono}"></i></span>
            </div>
            <div class="media-body">
                <h1 class="text-display-1 margin-none">${curso.titulo}</h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-9 col-md-8">

            <div class="page-section">
                <p>
                    ${curso.descripcion}
                </p>
            </div>
            <c:if test="${not empty curso.lecciones}">
                <div class="page-section">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-headline margin-none">Que vas a aprender</h2>
                            <p class="text-subhead text-light">Un resumen de los contenidos del curso.</p>
                            <ul class="list-group relative paper-shadow" data-hover-z="0.5" data-animated>
                                <c:forEach items="${curso.lecciones}" var="leccion">
                                    <li class="list-group-item">
                                        <div class="media v-middle">
                                            <div class="media-left">
                                                <div class="icon-block s30 bg-grey-500 text-white img-circle">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                            </div>
                                            <div class="media-body text-body-2">
                                                ${leccion.unidad.titulo}
                                            </div>
                                        </div>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>

                </div>
            </c:if>

        </div>
        <div class="col-lg-3 col-md-4">

            <div class="page-section">

                <!-- .panel -->
                <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                    <div class="panel-heading">
                        <h4 class="text-headline">Curso</h4>
                    </div>
                    <div class="panel-body">
                        <p class="text-caption">
                            <i class="fa fa-clock-o fa-fw"></i> ${curso.duracionHoras} hrs &nbsp;
                            <i class="fa fa-calendar fa-fw"></i> <fmt:formatDate value="${curso.fechaCreacion}" pattern="dd/MM/yyyy" />
                            <br/>
                            <i class="fa fa-user fa-fw"></i> Instructor: ${curso.creadoPor.username}
                            <br/>
                        </p>
                    </div>
                    <hr class="margin-none" />
                    <c:if test="${not empty curso.lecciones}">
                        <div class="panel-body text-center">
                            <p><a class="btn btn-success btn-lg paper-shadow relative" data-z="1" data-hover-z="2" data-animated href="${root}/cursos/${curso.id}/leccion/${primerLeccion.id}">Empezar Curso</a></p>
                        </div>
                    </c:if>
                    <div class="panel-body">
                        <a href="${root}/cursos/editar/${curso.id}" data-animated="" data-hover-z="1" data-z="0" class="btn btn-white btn-flat paper-shadow relative"><i class="fa fa-fw fa-pencil"></i> Editar curso</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
