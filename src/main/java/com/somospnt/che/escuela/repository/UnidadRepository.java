package com.somospnt.che.escuela.repository;

import com.somospnt.che.escuela.domain.Unidad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnidadRepository extends JpaRepository<Unidad, Long> {

}
