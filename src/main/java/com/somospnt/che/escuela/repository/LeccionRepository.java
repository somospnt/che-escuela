package com.somospnt.che.escuela.repository;

import com.somospnt.che.escuela.domain.Leccion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LeccionRepository extends JpaRepository<Leccion, Long> {

    @Query("select coalesce(max(l.orden), 0) from Leccion l where l.curso.id = ?")
    int findMaxOrdenByCurso(long idCurso);

}
