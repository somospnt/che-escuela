package com.somospnt.che.escuela.repository;

import com.somospnt.che.escuela.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CursoRepository extends JpaRepository<Curso, Long> {

}
