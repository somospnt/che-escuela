package com.somospnt.che.escuela.config;

import com.somospnt.che.escuela.CheEscuelaApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CheEscuelaApplication.class);
	}

}
