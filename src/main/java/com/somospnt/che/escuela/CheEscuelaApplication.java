package com.somospnt.che.escuela;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheEscuelaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheEscuelaApplication.class, args);
	}
}
