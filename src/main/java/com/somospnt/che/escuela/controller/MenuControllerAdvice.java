package com.somospnt.che.escuela.controller;

import com.somospnt.che.escuela.domain.Usuario;
import com.somospnt.che.escuela.service.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice(annotations = Controller.class)
public class MenuControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(MenuControllerAdvice.class);

    @Autowired
    private UsuarioService usuarioService;

    @ModelAttribute("usuarioLogeado")
    public Usuario buscarUsuario() {
        try {
            return usuarioService.buscarUsuarioLogueado();
        } catch (AccessDeniedException ex) {
            //si el usuario no esta logueado, continuar normalmente
            return null;
        } catch (Exception ex) {
            //si ocurre un error, loguear y devolver null para evitar loops infinitos
            logger.error("Error buscando usuario.", ex);
            return null;
        }
    }
}
