package com.somospnt.che.escuela.controller;

import com.somospnt.che.escuela.domain.Leccion;
import com.somospnt.che.escuela.service.LeccionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LeccionRestController {

    @Autowired
    private LeccionService leccionService;

    @RequestMapping(value = "/api/lecciones/orden", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void actualizarOrden(@RequestBody List<Leccion> lecciones) {
        leccionService.actualizarOrden(lecciones);
    }

}
