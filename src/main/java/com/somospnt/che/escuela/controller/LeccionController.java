package com.somospnt.che.escuela.controller;

import com.somospnt.che.escuela.domain.Curso;
import com.somospnt.che.escuela.domain.Leccion;
import com.somospnt.che.escuela.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LeccionController {

    @Autowired
    private CursoService cursoService;

    @RequestMapping("/cursos/{idCurso}/leccion/{idLeccion}")
    public String detalleCurso(@PathVariable long idCurso, @PathVariable long idLeccion, Model model) {
        Curso curso = cursoService.buscarPorId(idCurso);
        model.addAttribute("curso", curso);
        model.addAttribute("leccionActual", buscarLeccion(curso, idLeccion));
        return "detalleLeccion";
    }

    private Leccion buscarLeccion(Curso curso, long idLeccion) {
        return curso.getLecciones().stream()
                .filter(leccion -> leccion.getId() == idLeccion)
                .findFirst()
                .get();
    }
}
