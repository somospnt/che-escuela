package com.somospnt.che.escuela.controller;

import com.somospnt.che.escuela.domain.Curso;
import com.somospnt.che.escuela.domain.Leccion;
import com.somospnt.che.escuela.service.CursoService;
import com.somospnt.che.escuela.vo.ColorVo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @RequestMapping("/cursos")
    public String cursos(Model model) {
        model.addAttribute("cursos", cursoService.buscarTodos());
        return "cursos";
    }

    @RequestMapping("/cursos/{id}")
    public String detalleCurso(@PathVariable long id, Model model) {
        Curso curso = cursoService.buscarPorId(id);
        model.addAttribute("curso", curso);
        model.addAttribute("primerLeccion", obtenerPrimerLeccion(curso));
        return "detalleCurso";
    }

    private static Leccion obtenerPrimerLeccion(Curso curso) {
        if (CollectionUtils.isEmpty(curso.getLecciones())) {
            return null;
        }
        return curso.getLecciones().stream().findFirst().get();
    }

    @RequestMapping(value = "/cursos", method = RequestMethod.POST)
    public String guardar(@ModelAttribute Curso curso, Model model) {
        cursoService.guardar(curso);
        model.addAttribute("curso", cursoService.buscarPorId(curso.getId()));
        model.addAttribute("colores", ColorVo.COLORES_BASICOS);
        return "editarCurso";
    }

    @RequestMapping("/cursos/nuevo")
    public String nuevo(Model model) {
        model.addAttribute("curso", new Curso());
        model.addAttribute("colores", ColorVo.COLORES_BASICOS);
        return "editarCurso";
    }

    @RequestMapping("/cursos/editar/{id}")
    public String editar(@PathVariable long id, Model model) {
        model.addAttribute("curso", cursoService.buscarPorId(id));
        model.addAttribute("colores", ColorVo.COLORES_BASICOS);
        return "editarCurso";
    }

}
