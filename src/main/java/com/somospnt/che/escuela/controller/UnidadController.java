package com.somospnt.che.escuela.controller;

import com.somospnt.che.escuela.domain.Unidad;
import com.somospnt.che.escuela.service.LeccionService;
import com.somospnt.che.escuela.service.UnidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UnidadController {

    @Autowired
    private UnidadService unidadService;
    @Autowired
    private LeccionService leccionService;

    @RequestMapping("/unidades")
    public String unidades(Model model) {
        model.addAttribute("unidades", unidadService.buscarTodos());
        return "unidades";
    }

    @RequestMapping("/unidades/{idUnidad}")
    public String mostrarUnidad(@PathVariable long idUnidad, Model model) {
        model.addAttribute("unidad", unidadService.buscarPorId(idUnidad));
        return "detalleUnidad";
    }

    @RequestMapping("/unidades/nuevo")
    public String nuevo(@RequestParam(required = false) Long idCurso, Model model) {
        model.addAttribute("idCurso", idCurso);
        return "editarUnidad";
    }

    @RequestMapping("/unidades/editar/{idUnidad}")
    public String editar(@PathVariable long idUnidad, @RequestParam Long idCurso, Model model) {
        model.addAttribute("idCurso", idCurso);
        model.addAttribute("unidad", unidadService.buscarPorId(idUnidad));
        return "editarUnidad";
    }

    @RequestMapping(value = "/unidades", method = RequestMethod.POST)
    public String guardar(@ModelAttribute Unidad unidad, @RequestParam Long idCurso, Model model) {
        boolean esNueva = unidad.getId() == null;

        unidadService.guardar(unidad);
        if (esNueva && idCurso != null) {
            leccionService.guardar(unidad.getId(), idCurso);
        }

        model.addAttribute("idCurso", idCurso);
        model.addAttribute("unidad", unidad);
        return "editarUnidad";
    }

}
