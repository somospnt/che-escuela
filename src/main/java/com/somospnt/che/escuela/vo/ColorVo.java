package com.somospnt.che.escuela.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Colores usados para los cursos y otros artefactos de presentación.
 *
 */
public class ColorVo implements Comparable<ColorVo>{

    public static final List<ColorVo> COLORES_BASICOS = new ArrayList<>();

    static {
        Map<String, String> colores = new HashMap<>();
        colores.put("bg-amber", "Ambar");
        colores.put("bg-blue", "Azul");
        colores.put("bg-blue-grey", "Azul Gris");
        colores.put("bg-brown", "Marrón");
        colores.put("bg-cyan", "Cian");
//        colores.put("bg-deep-orange", "Naranja Profundo");
//        colores.put("bg-deep-purple", "Púrpura Profundo");
        colores.put("bg-green", "Verde");
//        colores.put("bg-grey", "Gris");
        colores.put("bg-indigo", "Indigo");
//        colores.put("bg-light-blue", "Azul Claro");
        colores.put("bg-light-green", "Verde Claro");
        colores.put("bg-lime", "Lima");
        colores.put("bg-orange", "Naranja");
        colores.put("bg-pink", "Rosa");
        colores.put("bg-purple", "Púrpura");
        colores.put("bg-red", "Rojo");
        colores.put("bg-teal", "Teal");
//        colores.put("bg-yellow", "Amarillo");

        for (String css : colores.keySet()) {
            COLORES_BASICOS.add(new ColorVo(css + "-400", colores.get(css) + " 400"));
            COLORES_BASICOS.add(new ColorVo(css + "-600", colores.get(css) + " 600"));
            COLORES_BASICOS.add(new ColorVo(css + "-800", colores.get(css) + " 800"));
        }

        Collections.sort(COLORES_BASICOS);
    }

    private String claseCss;
    private String nombre;

    private ColorVo(String claseCss, String nombre) {
        this.claseCss = claseCss;
        this.nombre = nombre;
    }

    public String getClaseCss() {
        return claseCss;
    }

    public void setClaseCss(String claseCss) {
        this.claseCss = claseCss;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int compareTo(ColorVo o) {
        return this.nombre.compareTo(o.getNombre());
    }

}
