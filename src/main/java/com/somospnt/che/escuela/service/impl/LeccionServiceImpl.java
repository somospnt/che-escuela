package com.somospnt.che.escuela.service.impl;

import com.somospnt.che.escuela.domain.Leccion;
import com.somospnt.che.escuela.repository.CursoRepository;
import com.somospnt.che.escuela.repository.LeccionRepository;
import com.somospnt.che.escuela.repository.UnidadRepository;
import com.somospnt.che.escuela.service.LeccionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LeccionServiceImpl implements LeccionService {

    @Autowired
    private LeccionRepository leccionRepository;
    @Autowired
    private UnidadRepository unidadRepository;
    @Autowired
    private CursoRepository cursoRepository;

    @Override
    public Leccion buscarPorId(long id) {
        return leccionRepository.findOne(id);
    }

    @Override
    @PreAuthorize("hasRole('INSTRUCTOR')")
    public Leccion guardar(long idUnidad, long idCurso) {
        Leccion leccion = new Leccion();
        leccion.setCurso(cursoRepository.findOne(idCurso));
        leccion.setUnidad(unidadRepository.findOne(idUnidad));
        int orden = leccionRepository.findMaxOrdenByCurso(idCurso);
        leccion.setOrden(++orden);
        leccionRepository.save(leccion);
        return leccion;
    }

    @Override
    @PreAuthorize("hasRole('INSTRUCTOR')")
    public void actualizarOrden(List<Leccion> lecciones) {
        for (Leccion leccion : lecciones) {
            Leccion leccionExistente = leccionRepository.findOne(leccion.getId());
            leccionExistente.setOrden(leccion.getOrden());
        }
    }

}
