package com.somospnt.che.escuela.service;

import com.somospnt.che.escuela.domain.Usuario;

public interface UsuarioService {
    
    Usuario buscarUsuarioLogueado();
    
}
