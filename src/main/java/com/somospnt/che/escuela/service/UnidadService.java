package com.somospnt.che.escuela.service;

import com.somospnt.che.escuela.domain.Unidad;
import java.util.List;

public interface UnidadService {

    Unidad buscarPorId(long id);
    void guardar(Unidad unidad);
    List<Unidad> buscarTodos();

}
