package com.somospnt.che.escuela.service.impl;

import com.somospnt.che.escuela.domain.Curso;
import com.somospnt.che.escuela.repository.CursoRepository;
import com.somospnt.che.escuela.service.CursoService;
import com.somospnt.che.escuela.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class CursoServiceImpl implements CursoService {

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public List<Curso> buscarTodos() {
        return cursoRepository.findAll();
    }

    @Override
    @PreAuthorize("hasRole('INSTRUCTOR')")
    public void guardar(Curso curso) {
        Curso cursoActual;
        if (curso.getId() == null) {
            cursoActual = curso;
            cursoActual.setCreadoPor(usuarioService.buscarUsuarioLogueado());
        } else {
            cursoActual = cursoRepository.findOne(curso.getId());
            cursoActual.setDescripcion(curso.getDescripcion());
            cursoActual.setTitulo(curso.getTitulo());
            cursoActual.setColor(curso.getColor());
            cursoActual.setDescripcionCorta(curso.getDescripcionCorta());
            cursoActual.setDuracionHoras(curso.getDuracionHoras());
            cursoActual.setIcono(curso.getIcono());
        }
        
        cursoRepository.save(cursoActual);
    }

    @Override
    public Curso buscarPorId(Long id) {
        return cursoRepository.findOne(id);
    }

}
