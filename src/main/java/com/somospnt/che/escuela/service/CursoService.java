package com.somospnt.che.escuela.service;

import com.somospnt.che.escuela.domain.Curso;
import java.util.List;

public interface CursoService {

    List<Curso> buscarTodos();

    Curso buscarPorId(Long id);

    void guardar(Curso curso);
}
