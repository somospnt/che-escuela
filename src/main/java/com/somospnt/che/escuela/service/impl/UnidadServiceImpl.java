package com.somospnt.che.escuela.service.impl;

import com.somospnt.che.escuela.domain.Unidad;
import com.somospnt.che.escuela.repository.UnidadRepository;
import com.somospnt.che.escuela.service.UnidadService;
import com.somospnt.che.escuela.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UnidadServiceImpl implements UnidadService {

    @Autowired
    private UnidadRepository unidadRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public Unidad buscarPorId(long id) {
        return unidadRepository.findOne(id);
    }

    @Override
    @PreAuthorize("hasRole('INSTRUCTOR')")
    public void guardar(Unidad unidad) {
        Unidad unidadActual;
        if (unidad.getId() == null) {
            unidadActual = unidad;
            unidadActual.setCreadoPor(usuarioService.buscarUsuarioLogueado());
        } else {
            unidadActual = unidadRepository.findOne(unidad.getId());
            unidadActual.setContenido(unidad.getContenido());
            unidadActual.setTitulo(unidad.getTitulo());
        }

        unidadRepository.save(unidadActual);
    }

    @Override
    public List<Unidad> buscarTodos() {
        return unidadRepository.findAll();
    }

}
