package com.somospnt.che.escuela.service;

import com.somospnt.che.escuela.domain.Leccion;
import java.util.List;

public interface LeccionService {

    Leccion buscarPorId(long id);
    Leccion guardar(long idUnidad, long idCurso);
    void actualizarOrden(List<Leccion> lecciones);

}
